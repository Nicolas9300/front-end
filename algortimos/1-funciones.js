function saludar(){
    console.log("hola");
}

saludar();
var saludarHumano = false;

if(saludarHumano){
    saludar();
}

function suma(a,b){
    console.log("resultado de a + b", a+b);
}

suma(3,6);
suma(6,8);

function sumaConResultado(num1,num2){
  return num1+num2;
}

//var resultado = sumaConResultado(10,4);
//console.log(resultado);

function revisarDescuento(producto){
    if(producto.precio >= 300){
        return "tienes descuento con " + producto.nombre;
    }else{
        return "no tienes descuento con " + producto.nombre;
    }
}

var productos=[
    {nombre: "Ps4", precio: 250},
    {nombre: "iphone", precio: 800},
    {nombre: "Laptop", precio: 350},
    {nombre: "TV", precio: 200}
];

for(var i = 0; i < productos.length; i++){
    var resultado = revisarDescuento(productos[i]);
    console.log(resultado);
}


