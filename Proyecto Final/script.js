var articulos = [
{
  id:"articulo-001",
  nombre: "perro salchicha",
  precio: 50,
  cover: "salchicha.jpg"
},
// {
//   id:"articulo-002",
//   nombre: "buzo buchino",
//   precio: 100,
//   cover: "ropa1.jpg"
// },
// {
//   id:"articulo-003",
//   nombre: "Vestido",
//   precio: 150,
//   cover: "ropa2.jpg"
// }

];

function crearElemento(tipo, contenido, clase,img){
 var elemento = document.createElement(tipo);

 if(contenido != null){
elemento.innerHTML = contenido;
 }
 if(clase !=null){
   elemento.classList.add(clase);
 }
 if(img != null){
   elemento.setAttribute("src","imagenes/"+ img);
 }

 return elemento;

}

function dibujarArticulo(articulo){
var itemCard = crearElemento("div",null,"item-card",null);

//agregar cover del articulo en el item card
itemCard.appendChild(crearElemento("img",null,"item-cover",articulo.cover));

//agregar titulo del articulo
itemCard.appendChild(crearElemento("h2",articulo.nombre,null,null));

var filaPrecio = crearElemento("div",null,"fila",null);

//agregar etiqueta precio en la fila
filaPrecio.appendChild(crearElemento("span","precio","subtitulo",null));

//agregar simbolo precio en la fila
filaPrecio.appendChild(crearElemento("span","$","precio",null));

//agregar valor precio en la fila
filaPrecio.appendChild(crearElemento("span",articulo.precio,"precio",null));

//agregar fila de precio en el item card
itemCard.appendChild(filaPrecio);

var filaContador = crearElemento("div",null,"fila",null);

//agregar etiqueta cantidad en la fila contador
filaContador.appendChild(crearElemento("span","cantidad","subtitulo",null));

var contador = crearElemento("div",null,"contador",null);


var numeroContador = crearElemento("span",1,"cantidad",null);
//agregar span en el contador
contador.appendChild(numeroContador);

var boton_Menos = crearElemento("img",null,"menos","signo-menos-de-una-linea-en-posicion-horizontal.png");
boton_Menos.addEventListener("click",disminuir.bind(null, numeroContador));
//agregar boton menos en contador
contador.appendChild(boton_Menos);

var boton_Mas = crearElemento("img",null,"mas","anadir.png");
boton_Mas.addEventListener("click",incrementar.bind(null, numeroContador));
//agregar boton mas en contador
contador.appendChild(boton_Mas);

filaContador.appendChild(contador);

//agregar fila de contador en item card
itemCard.appendChild(filaContador);

var boton_Agregar = crearElemento("div","Agregar","boton-agregar",null);
boton_Agregar.addEventListener("click",
agregar.bind(null,articulo,numeroContador));
//agregar boton de AGREGAR en item card
itemCard.appendChild(boton_Agregar);

var contenedorArticulos = document.getElementById("contenedor-articulos");
console.log(contenedorArticulos)
contenedorArticulos.appendChild(itemCard);
};


for (var i = 0; i < articulos.length; i++) {
  dibujarArticulo(articulos[i]);
  
}

//variables de la tarjeta total a pagar
var precioSubtotal =0;
var etiquetaSubtotal = document.getElementById("etiqueta-subtotal");
var etiquetaTotal = document.getElementById("etiqueta-total");
var contenedorEtiquetas =document.getElementById("contenedor-etiquetas");
var etiquetaEnvio = document.getElementById("etiqueta-envio");
var etiquetaDescuento = document.getElementById("etiqueta-descuento");
var valorEnvio = document.getElementById("valor-envio");
var valorDescuento = document.getElementById("valor-descuento");
var descuento = 0;


//condicion para descuento
//10% de descuento con mas de 500 dolares y comprar 2 articulos
//envio gratis con una compra de + de 200

function incrementar(referenciContador){
  referenciContador.innerHTML++;
}

function disminuir(referenciContador){
    if(referenciContador.innerHTML > 1){
      referenciContador.innerHTML--;
    }
}


function agregar(articulo,referenciContador){
  //marcar que este articulo se agrego
  articulo.agregado = true;
  precioSubtotal += articulo.precio * Number(referenciContador.innerHTML);
  etiquetaSubtotal.innerHTML = precioSubtotal;

  if(precioSubtotal >100){
    etiquetaEnvio.style.color = "#4382ff";
    valorEnvio.innerHTML = 0;
  }
  agregarEtiquetaArticulo(articulo, referenciContador);
  var aplicarDescuento = corroborarDescuento();

 console.log(aplicarDescuento);
  if(aplicarDescuento && precioSubtotal > 500){
    etiquetaDescuento.style.color = "#4382ff";
    descuento = precioSubtotal *0.1;
    valorDescuento.innerHTML = descuento;
  }

  etiquetaTotal.innerHTML = precioSubtotal + Number(valorEnvio.innerHTML) - descuento;
  referenciContador.innerHTML = 1;
}

function agregarEtiquetaArticulo(articulo,referenciContador){
 var fila =crearElemento("div",null,"fila",null);
 
 if(articulo.cantidadAcumulada == null){
   articulo.cantidadAcumulada = Number(referenciContador.innerHTML);
 }else{
   articulo.cantidadAcumulada += Number(referenciContador.innerHTML);
 }

 var texto =crearElemento("span",articulo.nombre + " x "+ articulo.cantidadAcumulada, "subtitulo",null);


 fila.appendChild(texto);
if(articulo.referenciaArticulo == null){
  contenedorEtiquetas.appendChild(fila);
}else{
  contenedorEtiquetas.replaceChild(fila,articulo.referenciaArticulo);
}
 articulo.referenciaArticulo = fila;
}

function corroborarDescuento(){
 var cantidadTipoArticulos = 0;

 for (var i = 0; i < articulos.length; i++) {
   if(articulos[i].agregado){
     cantidadTipoArticulos++;
   }
   
 }

 if(cantidadTipoArticulos >= 2){
   return true;
 }else{
   return false;
 }

}
