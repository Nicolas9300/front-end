const express = require('express');

const app = express();


app.get("/",inicio);


app.use(express.static("pagina"));
app.use("/static", express.static("pagina/salisHoy"));

function inicio (req ,res){
    res.sendFile(__dirname + "/salisHoy.html")
}


app.listen(3000, ()=>{

    console.log('Server en puerto 3000')
})