let listElements = document.querySelectorAll('.list_buttom');

listElements.forEach(listElement => {
   listElement.addEventListener('click',()=>{
     
       listElement.classList.toggle('arrow');

      let height = 0;
      let menu = listElement.nextElementSibling;

      if(menu.clientHeight == "0"){
        height = menu.scrollHeight;
      }

      menu.style.height = `${height}px`;
   })
});


const btns = document.querySelectorAll(".btn_filtro");
const products = document.querySelectorAll(".tarjeta");
let listaEtiquetas = null;
btns.forEach(btn =>{

  btn.addEventListener('click',(e)=>{
         e.preventDefault();

         let filtro = e.target.dataset.filter;
        
  
         products.forEach(product =>{
          if(btns[0] == btn){
            filtro = document.querySelector(".buscador").value;
          }
            if(!product.classList.contains(filtro.toLowerCase())){
              product.classList.replace('tarjeta','desaparecer');
            }
         });
         crearCategoria(filtro);
         document.querySelector(".buscador").value = "";

         listaEtiquetas = document.querySelectorAll(".category_name");
         

         listaEtiquetas.forEach(etiqueta=>{
    
          etiqueta.addEventListener('click',(e)=>{

            let palabra = e.target.innerText

            sacarFiltro(palabra,etiqueta);
          
            
            
          })
        })
        
  });

 

});

function sacarFiltro(palabra,etiqueta){
  
  products.forEach(product=>{
    if(!product.classList.contains(palabra.toLowerCase())){
      product.classList.replace('desaparecer','tarjeta');
    }
  })
     let padre = document.querySelector(".category_name").parentNode;

      while (padre.firstChild){
        padre.removeChild(padre.firstChild);
      };
     


}




function crearCategoria(category){

  const listaCategoria = document.querySelectorAll(".category_name");
  let repetido = false;
  //console.log(listaCategoria[0].childNodes[0].innerHTML)

  listaCategoria.forEach(categoriaVasia =>{
    if(categoriaVasia.childNodes[0].innerHTML.toLowerCase() == category.toLowerCase())
       repetido = true;
  });

  if(!repetido){
    if(category != ""){
    //Crea el div
    let elemento = document.createElement("div");
    elemento.classList.add("category_name");
    //Span
    let categoriaNombre = document.createElement("span");
    categoriaNombre.innerHTML = category;
    //Img
    let x = document.createElement("img");
    x.setAttribute("src","../imagenes/equisd.png");

    elemento.appendChild(categoriaNombre);
    elemento.appendChild(x);
    

    let padre = document.getElementById("filter_container--mini")
    padre.appendChild(elemento);

    }
  }
}






