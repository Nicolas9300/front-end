let informaciones = [

{
  id: "info-001",
  titulo: "Todo sobre el cafe",
  resumen:"La harina es un polvo fino que se obtiene del cereal molido,es uno de los ingredientes más utilizados para preparar alimentos. Si bien...",
  imagen: "cafe.jpg",
  enlace: "file:///C:/Users/Equipo/Desktop/Front%20y%20Back%20End/IntentoDePaginaMorfi/paginaPrincipal/index.html"
},

{
  id: "info-001",
  titulo: "Todo sobre el ",
  resumen:"La harina es un polvo fino que se obtiene del cereal molido,es uno de los ingredientes más utilizados para preparar alimentos. Si bien...",
  imagen: "cafe.jpg",
  enlace: "file:///C:/Users/Equipo/Desktop/Front%20y%20Back%20End/IntentoDePaginaMorfi/paginaPrincipal/index.html"
},

{
  id: "info-001",
  titulo: "Todo ",
  resumen:"La harina es un polvo fino que se obtiene del cereal molido,es uno de los ingredientes más utilizados para preparar alimentos. Si bien...",
  imagen: "cafe.jpg",
  enlace: "file:///C:/Users/Equipo/Desktop/Front%20y%20Back%20End/IntentoDePaginaMorfi/paginaPrincipal/index.html"
},

{
  id: "info-001",
  titulo: "Todo ",
  resumen:"La harina es un polvo fino que se obtiene del cereal molido,es uno de los ingredientes más utilizados para preparar alimentos. Si bien...",
  imagen: "cafe.jpg",
  enlace: "file:///C:/Users/Equipo/Desktop/Front%20y%20Back%20End/IntentoDePaginaMorfi/paginaPrincipal/index.html"
}

];


function crearElemento(tipo, contenido,href, clase,img){
  var elemento = document.createElement(tipo);
 
  if(contenido != null){
    elemento.innerHTML = contenido;
  }
  if(href != null){
    elemento.setAttribute("href",href);
  }
  if(clase !=null){
    elemento.classList.add(clase);
  }
  if(img != null){
    elemento.setAttribute("src","../imagenes/"+ img);
  }
 
  return elemento;
 
 }

function dibujarInfo(informacion){
  var articulo = crearElemento("div",null,null,"nota",null);

  var enlace = crearElemento("a",null,informacion.enlace,null,null);

  enlace.appendChild(crearElemento("img",null,null,"imagen-cuadro",informacion.imagen));
  
  articulo.appendChild(enlace);

  var tituloFoto  = crearElemento("div",null,null,"derecha",null);

  tituloFoto.appendChild(crearElemento("h2",informacion.titulo,null,null,null));

  var infoFoto = crearElemento("div",null,null,"resumen",null);

  infoFoto.appendChild(crearElemento("span",informacion.resumen,null,null,null));

  tituloFoto.appendChild(infoFoto);

  articulo.appendChild(tituloFoto);

  var contenedorArticulos = document.getElementById("rectangulo");
  
  contenedorArticulos.appendChild(articulo);

};

for(var i = 0; i < informaciones.length; i++){
  dibujarInfo(informaciones[i]);

}