var boton = document.getElementById("boton");
var contador = 0;
boton.addEventListener("click",agregarBoton);

function agregarBoton(){
    var botonhijo = document.createElement("div");
    var numeroboton = "Elemento hijo " + contador;
    
    botonhijo.innerHTML = numeroboton;
    botonhijo.classList.add("boton-hijo");
    contador++;
    botonhijo.addEventListener("click",saludar.bind(null,numeroboton));

    document.body.appendChild(botonhijo);
}

function saludar(nombre,e){
    console.log("hola soy " + nombre);
    e.target.style.background = "red";
}